# STLC-MiniKanren

A type/term/context inference program for Simply Typed Lambda Calculus implemented using MiniKanren, Scheme and [Haskeme](https://github.com/jumper149/haskeme).

Look at the source for more details.
