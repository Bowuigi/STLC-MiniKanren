;;;; Simply typed lambda calculus

;;; Standard typing rules

;; x : σ ∈ Γ
;; --------- Env
;; Γ ⊢ x : σ
;;
;; const c : T
;; ----------- Const
;;  Γ ⊢ c : T
;;
;;      Γ, x : σ ⊢ e : τ
;; ------------------------- Lam
;; Γ ⊢ (λx : σ. e) : (σ → τ)
;;
;; Γ ⊢ e₁ : σ → τ   Γ ⊢ e₂ : σ
;; --------------------------- App
;;        Γ ⊢ e₁ e₂ : τ

;;; Initial setup

;; const unit : Unit

define (memberO x lst)
  fresh (y rest)
    == `(,y . ,rest) lst
    conde
      (== y x)
      (=/= y x) (memberO x rest)

define (lookupO Γ var type)
  memberO `(,var . ,type) Γ

define (inferO Γ term type)
  conde
    ;; Env
    (symbolo term) (lookupO Γ term type)
    ;; Lam
    (fresh (x body t-x t-body)
      == `(λ ,x ,body) term
      symbolo x
      == `(,t-x -> ,t-body) type
      inferO `((,x . ,t-x) . ,Γ) body t-body)
    ;; App
    (fresh (rator rand t-rand)
      == `(,rator ,rand) term
      inferO Γ rator `(,t-rand -> ,type)
      inferO Γ rand t-rand)

define (infer-type-display Γ term)
  display "[type] "
  display term
  display " with env "
  display Γ
  display " of type "
  display
    let ((res (run 1 (type) (inferO Γ term type))))
      if (null? res)
        res
        car res
  newline

define (infer-Γ-display term type)
  display "[Γ] "
  display term
  display " with env "
  display
    let ((res (run 1 (Γ) (inferO Γ term type))))
      if (null? res)
        res
        car res
  display " of type "
  display type
  newline

define (infer-term-display Γ type)
  display "[term] "
  display
    let ((res (run 1 (term) (inferO Γ term type))))
      if (null? res)
        res
        car res
  display " with env "
  display Γ
  display " of type "
  display type
  newline

define (infer-display term)
  infer-type-display '((unit . Unit)) term

define (main args)
  infer-display 'unit
  infer-display '(λ x x)
  infer-display '((λ x x) unit)
  infer-display '(λ x unit)
  infer-display '(λ x (λ y unit))
  infer-display '(λ x (λ y x))
  infer-display '(λ x (λ y y))
  infer-display '(λ x (λ y (λ z ((x z) (y z)))))
  infer-display '(λ x (λ y (x y)))
  infer-Γ-display 'false 'Bool
  infer-Γ-display '(λ x true) '(a -> Bool)
  infer-term-display '((unit . Unit)) 'Unit
  infer-term-display '((unit . Unit)) '(a -> Unit)
  infer-term-display '() '(a -> a)
  infer-term-display '() '(a -> (b -> a))
  infer-term-display '((0 . Nat) (1 . Nat)) '(Nat -> Nat)
